import 'dart:convert';

Item itemFromJson(String str) {
  final jsonData = json.decode(str);
  return Item.fromMap(jsonData);
}

String itemToJson(Item data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}


class Item {
  int id;
  String title;
  String source;
  String link;

  Item({
    this.id,
    this.title,
    this.source,
    this.link,
  });

  factory Item.fromMap(Map<String, dynamic> json) => new Item(
    id: json["id"],
    title: json["title"],
    source: json["source"],
    link: json["link"]
  );

  Map<String, dynamic> toMap() => {
    "id": id,
    "first_name": title,
    "last_name": source,
    "link": link,
  };
}
