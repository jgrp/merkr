import 'dart:async';

import '../models/item.dart';
import '../database.dart';


class ItemsBloc {
  ItemsBloc() {
    getItems();
  }
  final _itemController =  StreamController<List<Item>>.broadcast();
  get items => _itemController.stream;

  dispose() {
    _itemController.close();
  }

  getItems() async {
    _itemController.sink.add(await DBProvider.db.getAllItems());
  }

  delete(int id) {
    DBProvider.db.deleteItem(id);
    getItems();
  }

  add(Item item) {
    print('add');
    DBProvider.db.newItem(item);
    getItems();
  }

  deleteAll() {
    DBProvider.db.deleteAll();
    getItems();
  }
}
