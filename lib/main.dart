import 'package:flutter/material.dart';

import 'database.dart';
import 'widgets/ItemWidget.dart';
import 'models/item.dart';
import 'BLOCS/DatabaseBloc.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        // primarySwatch: Colors.deepPurple,
        accentColor: Colors.deepPurple,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
        brightness: Brightness.dark,
      ),
      home: MyHomePage(title: 'Merkr'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController _inputTitle;
  TextEditingController _inputSource;

  initState() {
    _inputTitle = new TextEditingController();
    _inputSource = new TextEditingController();
    super.initState();
  }

  final bloc = ItemsBloc();

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }


  void _addItem() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      showDialog(
        context: context,
        builder: (_) => new SimpleDialog(
          title: new Text("Add item"),
          contentPadding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
          children: [
            new TextField(
              decoration: new InputDecoration(labelText: "Titel"),
              controller: _inputTitle,
            ),
            new TextField(
              decoration: new InputDecoration(labelText: "Quelle"),
              controller: _inputSource,
            ),
            new FlatButton(
              onPressed: () async {
                bloc.add(Item(
                  title: _inputTitle.text,
                  source: _inputSource.text
                ));
                setState(() {});

                Navigator.pop(context);
                _inputTitle.text = '';
                _inputSource.text = '';
              },
              child: new Text('Save')
            )
          ]
        )
      );
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.refresh),
              onPressed: () {
                bloc.getItems();
                setState(() {});
              }
          ),
          IconButton(
            icon: Icon(Icons.add),
            onPressed: _addItem
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
        // Column is also a layout widget. It takes a list of children and
        // arranges them vertically. By default, it sizes itself to fit its
        // children horizontally, and tries to be as tall as its parent.
        //
        // Invoke "debug painting" (press "p" in the console, choose the
        // "Toggle Debug Paint" action from the Flutter Inspector in Android
        // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
        // to see the wireframe for each widget.
        //
        // Column has various properties to control how it sizes itself and
        // how it positions its children. Here we use mainAxisAlignment to
        // center the children vertically; the main axis here is the vertical
        // axis because Columns are vertical (the cross axis would be
        // horizontal).
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            height: 500,
            child: StreamBuilder<List<Item>> (
                stream: bloc.items,
              builder: (BuildContext context, AsyncSnapshot<List<Item>> snapshot) {
              print(snapshot);
                if (snapshot.hasData) {
                return ListView.builder(
                            itemCount: snapshot.data.length,
                            itemBuilder: (BuildContext context, int index) {
                              return ItemWidget(snapshot.data[index]);
                            },
                          );
                } else {
                  return Center(child: Text('List empty or could not be loaded'));
                }
              }
            )
          )
        ]
      )
    )

   );
  }
}
