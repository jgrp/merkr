import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../BLOCS/DatabaseBloc.dart';
import '../models/item.dart';

class ItemWidget extends StatelessWidget {
  final Item item;

  ItemWidget(this.item);

  final bloc = ItemsBloc();

  void _onPressDelete () {
    bloc.delete(this.item.id);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(5.0),
        width: double.infinity,
        child: Card(
          child: Padding(
            padding: EdgeInsets.all(10.0),
            child: Row(
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      this.item.title,
                      style: TextStyle(
                        fontWeight: FontWeight.bold
                      )
                    ),
                    Text(this.item.source),
                    Text(this.item.id.toString())
                  ],
                ),
                Spacer(flex: 1),
                IconButton(
                  icon: Icon(Icons.remove),
                  onPressed: _onPressDelete,
                )
              ]
            ),
          ),
        )
    );
  }

}
