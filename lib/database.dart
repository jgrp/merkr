import 'dart:async';
import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

import 'models/item.dart';

class DBProvider {
  // create as singelton
  DBProvider._();
  static final DBProvider db = DBProvider._();
  // https://medium.com/flutter-community/using-sqlite-in-flutter-187c1a82e8b

  static Database _database;

  Future<Database> get database async {
      if(_database != null)
      return _database;

      // if _database is null we instantiate it
      _database = await initDB();
      return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "MerkrDB.db");
    return await openDatabase(path, version: 1, onOpen: (db) {},
      onCreate: (Database db, int version) async {
      await db.execute(
        "Create TABLE Item ("
          "id INTEGER PRIMARY KEY,"
          "title TEXT,"
          "source TEXT,"
          "link TEXT"
        ")");
    });
  }

  newItem(Item newItem) async {
    final db = await database;
    //get the biggest id in the table
    var table = await db.rawQuery("SELECT MAX(id)+1 as id FROM Item");
    int id = table.first["id"];
    //insert to the table using the new id
    var raw = await db.rawInsert(
        "INSERT Into Item (id,title,source,link)"
            " VALUES (?,?,?,?)",
        [id, newItem.title, newItem.source, newItem.link]);
    return raw;
  }

  Future<List<Item>> getAllItems() async {
    final db = await database;
    var res = await db.query("Item");
    List<Item> list =
    res.isNotEmpty ? res.map((c) => Item.fromMap(c)).toList() : [];
    return list;
  }

  deleteItem(int id) async {
    final db = await database;
    db.delete("Item", where: "id = ?", whereArgs: [id]);
  }

  deleteAll() async {
    final db = await database;
    db.rawDelete("Delete * from Item");
  }

}


